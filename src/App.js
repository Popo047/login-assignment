import { Route, Switch } from "react-router-dom";
import "./App.css";
import SignUpPage from "./pages/SignUpPage";
import Profile from "./pages/ProfilePage";
import SignInPage from "./pages/SignInPage";
import Mockoon from "./components/Mockoon";

function App() {
  return (
    <div className="App">
      {/* <Switch>
        <Route path="/" component={SignUpPage} />
        <Route path="/signin" component={SignInPage} />
        <Route path="/profile" component={Profile} />
      </Switch> */}
      <Mockoon />
    </div>
  );
}

export default App;

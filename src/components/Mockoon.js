import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { mockoonapiActions, mockoonapiReq } from "../store/mockoonapi-slice";

function Mockoon() {
  const dispatch = useDispatch();
  const apiResponse = useSelector((state) => state.mock.users);

  const fetchHandler = () => {
    dispatch(mockoonapiReq(1));
    dispatch(mockoonapiActions.isLoaded());
    console.log(apiResponse);
  };

  return (
    <>
      <div>
        <button onClick={fetchHandler}>Fetch</button>
      </div>
    </>
  );
}

export default Mockoon;

import { configureStore } from "@reduxjs/toolkit";
import authReducer from "./auth-slice";
import mockoonReducder from "./mockoonapi-slice";

const store = configureStore(
  {
    reducer: {
      auth: authReducer,
      mock: mockoonReducder,
    },
  },
  +window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;

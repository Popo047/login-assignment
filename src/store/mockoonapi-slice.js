import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
  users: [],
  isLoaded: false,
};

export const mockoonapiReq = createAsyncThunk(
  "mockoonapi/mockoonapiReq",
  async (id) => {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/users/${id}`
    );
    return response.json();
  }
);

const mockoonapi = createSlice({
  name: "mockoonapi",
  initialState,
  reducers: {
    isLoaded(state) {
      state.isLoaded = true;
    },
  },
  extraReducers: {
    [mockoonapiReq.fulfilled]: (state, action) => {
      state.users = action.payload;
    },
  },
});

export const mockoonapiActions = mockoonapi.actions;

export default mockoonapi.reducer;
